$(function () {
    $(window).scroll(function () {
        var scroll = $(window).scrollTop();
        if (scroll >= 100) {
            $(".menu").addClass("bg-blue nav-shadow");
        } else {
            $(".menu").removeClass("bg-blue nav-shadow");
        }
    });
    $(window).on('scroll', function () {
        // if the scroll distance is greater than 100px
        if ($(window).scrollTop() > 50) {
            // do something
            $('.menu').addClass('bg-blue');
        }
    });
});
$('.navbar-nav .nav-item .nav-link').click(function () {
    $('.navbar-nav .nav-item .nav-link.active').removeClass('active');
    $(this).addClass('active');
});

$(document).on("click", ".nav-item", function () {
    jQuery(".nav-item").closest(".bsnav-mobile").removeClass("in");
    jQuery(".toggler-spring").removeClass("active");
});

$(window).scroll(function () {
    var href = $(this).scrollTop();
    $('.link').each(function (event) {
        if (href >= $($(this).attr('href')).offset().top - 1) {
            $('.navbar-nav .nav-item.active').removeClass('active');
            $(this).addClass('active');
        }
    });
});



AOS.init();
